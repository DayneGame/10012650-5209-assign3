# Choosing the Right Frameworks
# Pool of Frameworks (7):
1. Bootstrap v4
2. Materialize CSS (Google's Design Framework)
3. Angular.js
4. Sematic-UI
5. Skeleton CSS
6. Foundation
7. Pure
# Why I choose these Frameworks (3):

## Bootstrap v4:
I choose Bootstrap v4 because my website is going to be used mainly on smart phones, and tablets. Bootstrap is the #1 most popular front-end component libary for responsive design. My website is also column base and Bootstrap has a very good column grid-layout, it has 12 built on responsive column grid-layouts, that are very beneficial for my design. Bootstrap also makes it very easy for making the website with its built in classes. Having built in classes makes the CSS a lot smaller, because you aren't writing as much styles. I have also used Bootstrap before and I find it very easy to use.

## Materialize CSS (Google's Design Framework):
Materialize CSS has a ready made rich layouts and animations, This makes for a powerful UI (user Interface). Materialize also has 12 built-in column layouts just like Bootstrap v4, which gives for a great grid-layout framework. Have a grid-layout framework is a must have for my website as it uses multiple columns. Materialize is also a responsive framework, for smart devices and tablets.

## Skeleton CSS
I chose Skeleton CSS because it is a very small and dead simple framework. Skeleton doesn't have a complex complicated CSS file, in fact it is only 400 lines long. Skeleton has 12 built-in grid templates. Skeleton is also a responsive framework so it can work with smart devices and tablets. There is only one problem with this framework is that since the framework isn't rich in CSS, majority of it still needs to be developed/made by hand. This won't be a massive problem for my website because it will only utilize the responsiveness and grid layout from the framework.

# Finale Design Framework (1)
The framework i'm going to be using is Skeleton CSS. The reason I have choosen this framework, is because it's small and dead simple, and allows me room to customize. Skeleton CSS also has a very excellent quick load time that is crucial for my website. This is because if you are in a hurry because of an emergency, you don't want to wait for the website to load, you want to to appear near instant. Another reason I choose this over the other frameworks is because, the libraries are massive in those frameworks and im not going to be able to utilize/use all of it. Skeleton CSS also offers a very simple and good responsive layout. The layout isn't as good as Bootstrap v4, but is simple enough to use and for me to customize. Skeleton CSS is the best framework that fits my needs perfectly and doesn't offer less or to much. 

#Learning Difficulty with Framework (Skeleton.css)
I didn't end up using a whole lot of features from this framework. When I researched the framework it seemed to have everything that I needed, and everything I could use. I then started using the framework but it would compromise other features and parts of the website. For example the responsive-ness. It didn't fit to my original design, so therefore I had to create my own media queries for both Mobile and Tablet designs. I used the framework very rarely throught the website. When I used the framework it was only for the button styles and containers, and even I changed those slightly. After using Skeleton.css, I think that I should of gone for something a bit bigger like bootstrap v4.

## What did I use Skeleton.css for?
I used Skeleton.css for a few containers, and every button that is in the website has some sort of functionality and design from skeleton.css

#What Couldn't I complete?
I could complete all of it, but I did have to change the layout slightly for tablet landscape and some other pages. 


