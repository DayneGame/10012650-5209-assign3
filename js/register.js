// Input boxes and button
const name = document.querySelector('.name-box');
const phone = document.querySelector('.ph-box');
const password = document.querySelector('.pw-box');
const password2 = document.querySelector('.pw2-box');
const submit = document.querySelector('.submit-button-login');

// functions 

var counter = 0;

var Name = function() {
    if (name.value == "") {
        counter = 0;
        alert("You didn't enter a Name!")
    } else {
        counter += 1;
        console.log(counter);
    }
}

var Phone = function() {
    if (phone.value == "") {
        counter = 0;
        alert("You didn't enter a Phone Number!")
    } else {
        counter += 1;
        console.log(counter);
    }
}

var Password = function() {
    if (password.value == "") {
        counter = 0;
        alert("You need to enter a Password!")
    } else {
        counter += 1;
        console.log(counter);
    }
}

var ReEnterPassword = function() {
    if (password2.value != password.value) {
        counter = 0;
        alert("Passwords Don't Match")
    } else {
        counter += 1;
        console.log(counter);
    }
}
var Check = function() {
    if (counter == 4) {
        alert("Congradulations, You can now use Find my Whanau!")
    }
}

submit.addEventListener('click', function() {
    Name();
    Phone();
    Password();
    ReEnterPassword();
    Check();
})