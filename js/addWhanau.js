// Input boxes and button
const name = document.querySelector('.name-box');
const phone = document.querySelector('.ph-box');
const address= document.querySelector('.address-box');
const ID = document.querySelector('.ID-box');
const password = document.querySelector('.pw-box');
const submit = document.querySelector('.submit-button-login');

// functions

var counter = 0;

var Name = function() {
    if (name.value == "") {
        counter = 0;
        alert("You didn't enter a Name!")
    } else {
        counter += 1;
        console.log(counter);
    }
}
var Phone = function() {
    if (phone.value == "") {
        counter = 0;
        alert("You didn't enter a Phone Number!")
    } else {
        counter += 1;
        console.log(counter);
    }
}
var Address = function() {
    if (address.value == "") {
        counter = 0;
        alert("You didn't enter an Address")
    } else {
        counter += 1;
        console.log(counter);
    }
}
var IDNum = function() {
    if (ID.value == "") {
        counter = 0;
        alert("You didn't enter an ID Number!")
    } else {
        counter += 1;
        console.log(counter);
    }
}
var Password = function() {
    if (password.value == "") {
        counter = 0;
        alert("You didn't enter a Password")
    } else {
        counter += 1;
        console.log(counter);
    }
}
var Check = function() {
    if (counter == 5) {
        alert("Congradulations, You have successfully added a Whanau Member")
    }
}

submit.addEventListener('click', function() {
    Name();
    Phone();
    Address();
    IDNum();
    Password();
    Check();
})