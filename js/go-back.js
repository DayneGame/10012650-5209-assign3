// ------------------------------------------------------------------- //
// ------------------------- Go Back Function ------------------------ //
// ------------------------------------------------------------------- //

// Button 
const backBtn = document.querySelector('.cross-btn'); 

// Function
function GoBack() {
    window.history.back(); // This show the previous page the user was last on
}

// add.EventListener so the functions deploys on button click 
backBtn.addEventListener('click', function() {
    GoBack();
})