// ------------------------------------------------------------------- //
// ------------------- NavBar Functions ------------------------------ //
// ------------------------------------------------------------------- //

document.querySelector('.toggler').addEventListener('click', function() {
    navWidth = document.querySelector('#mynavbar').style.width;
    if (navWidth == "250px") {
        return closeNav();
    }
    return openNav();
})

function openNav() {
    document.querySelector('#mynavbar').style.width = "250px";
    document.querySelector('#mynavbar').style.borderRight = "5px solid black";

}
function closeNav() {
    document.querySelector('#mynavbar').style.width = "0";
    document.querySelector('#mynavbar').style.borderRight = "0";
}



