
// ------------------------------------------------------------------- //
// ------------------- Preset buttons Functions ---------------------- //
// ------------------------------------------------------------------- //

// preset-buttons

const preset1btn = document.querySelector('#p1');
const preset2btn = document.querySelector('#p2');
const preset3btn = document.querySelector('#p3');
const phonebtn = document.querySelector('#ph');

// Preset button values! //
var buttonValue = "";

// Preset btn functions //

preset1btn.addEventListener('click', function() {
    alert("Everyone Ok? Message has been Sent!");
    buttonValue = "Everyone Ok?";
    console.log(buttonValue);
})
preset2btn.addEventListener('click', function() {
    alert("I'm Coming! Message has been Sent!");
    buttonValue = "I'm Coming!";
    console.log(buttonValue);
})
preset3btn.addEventListener('click', function() {
    alert("I can't get to You! Message has been Sent!");
    buttonValue = "I can't get to You!";
    console.log(buttonValue);
})
phonebtn.addEventListener('click', function() {
    window.open('tel:02102993594', '_self');
})