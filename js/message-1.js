
// Buttons > Constants

// Safe Button
const safeBtn = document.querySelector('.safe-medium-box');
// Panic Button
const panicBtn = document.querySelector('.not-safe-medium-box');
// Message Buttons
const message1 = document.querySelector('.mess-btn1');
const message2 = document.querySelector('.mess-btn2');
const message3 = document.querySelector('.mess-btn3');

// Functions that get deployed when user clicks

safeBtn.addEventListener('click', function() {
    alert('Your Status has been set to SAFE!')
})
panicBtn.addEventListener('click', function() {
    alert('Your Status has been set to PANIC!')
})
message1.addEventListener('click', function() {
    alert('Everyone Ok? Has been Sent!')
})
message2.addEventListener('click', function() {
    alert("I'm Coming! Has been Sent!")
})
message3.addEventListener('click', function() {
    alert("I can't get to you! Has been sent!")
})